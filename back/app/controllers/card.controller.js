const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');

const STATUS = require('../_constants/status');
const CardModel = require('../models/card.model');
const dbContext = require('../services/db-context.service');

const multer = require('multer');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __absolutePath + '/files/')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

var upload = multer({storage: storage});

router.use(expressValidator());

// CARD CREATE : po horowemu nujno middle postavit auth.verifyToken
router.post('/', upload.array("uploads[]", 12), (req, res) => {

    console.log('req.files: ', req.files);
    console.log('req.body: ', req.body);

    let uploadedFile = {};

    if (req.files.length > 0) {
        uploadedFile = {
            fileName: req.files[0].originalname,
            url: '/api/files/' + req.files[0].filename
        };
    }

    // VALIDATION : po idee zdes podrugomu nujno zamutit prosto na skoruyu ruku
    req.checkBody('title', 'title is required').notEmpty();
    req.checkBody('desc', 'msg is required').notEmpty();
    let errors = req.validationErrors();

    if (errors) {
        res.status(STATUS.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    let cObj = new CardModel({
        title: req.body.title,
        desc: req.body.desc,
        img: uploadedFile
    });

    dbContext.insertOne('cards', cObj, (err, result) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send("There was a problem creating the card.");

        console.log('card created in our system', result.ops);

        res.status(STATUS.SUCCESS).send({result: result});
    });

});

// GET list of cards :
router.get('/', (req, res) => {
    const perPage = +req.query.page_size || 10;
    let page = +req.query.page || 1;

    let query = {
        perPage: perPage,
        page: page
    };

    dbContext.find('cards', query, {}, (err, items) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send({error: 'server error'});
        if (!items) return res.status(STATUS.NOT_FOUND).send({error: 'cards not found'});

        dbContext.count('cards', {isDeleted: false}).then(count=>{
            let pages = Math.ceil(count / perPage);

            res.status(STATUS.SUCCESS).send({
                items: items,
                pages,
                count
            });
        });
    });

});

router.put('/', (req, res) => {

    // VALDATION :
    req.checkBody('cardId', 'cardId is required').notEmpty();
    let errors = req.validationErrors();

    if (errors) {
        res.status(STATUS.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    CardModel.findByIdAndUpdate(req.body.cardId, req.body, (err, result) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send("There was a problem update cards.");

        res.status(STATUS.SUCCESS).send({msg: 'cards has been updated.'});

    });
});

router.delete('/', (req, res) => {
    // VALIDATION :
    req.checkQuery('cardId', 'cardId is required').notEmpty();
    let errors = req.validationErrors();

    if (errors) {
        res.status(STATUS.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    let cardId = req.query.cardId;

    CardModel.findByIdAndUpdate(cardId, {isDeleted: true}, (err, result) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send("There was a problem deleting cardId.");

        res.status(STATUS.SUCCESS).send({msg: 'cardId has been deleted.'});

    });
});

// GET card by cardId
router.get('/detail/:id', (req, res) => {
    const cardId = req.params.id;

    dbContext.findOne('cards', cardId, (err, items) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send({error: 'server error'});
        if (!items) return res.status(STATUS.NOT_FOUND).send({error: 'cards not found'});

        res.status(STATUS.SUCCESS).send(items);
    });

});


module.exports = router;
