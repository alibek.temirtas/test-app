const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');
const fs = require('fs');

router.use(expressValidator());

// get image by name
router.get('/img/:filename', (req, res) => {
  let filePath = __absolutePath+'/images/'+req.params.filename;

  fs.stat(filePath, (err, stat) => {
    if(err == null) {
      res.download(filePath);
    }  else {
      console.log('Some other error: ', err.code);
      res.status(404).send({error: 'file not found'});
    }
  });

});

// get image by name
router.get('/:filename', (req, res) => {
  let filePath = __absolutePath+'/files/'+req.params.filename;

  fs.stat(filePath, (err, stat) => {
    if(err == null) {
      res.download(filePath);
    }  else {
      console.log('Some other error: ', err.code);
      res.status(404).send({error: 'file not found'});
    }
  });

});

module.exports = router;
