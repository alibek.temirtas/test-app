const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');

const db = require('../db');
const DbEntity = require('./dbentity.model');

class CardModel extends DbEntity {
    constructor(obj) {
        super();
        this.title = obj.title;
        this.desc = obj.desc;
        this.img = obj.img || null;
    }


    static findByIdAndUpdate(cardId, formdata, cb) {
        db.get().collection('cards').updateOne({_id: ObjectID(cardId)},
            {
                $set: {
                    ...formdata,
                    updatedDate: moment.now()
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }
}

module.exports = CardModel;
