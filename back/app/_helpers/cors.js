const config = require('../_constants/_config');

function makeAccessControl(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', config.client_url);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
}

module.exports = {
  makeAccessControl: makeAccessControl
};
