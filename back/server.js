const http = require('http');

const app = require('./app/app');
const db = require('./app/db');
const config = require('./app/_constants/_config');

const PORT_HTTP = 3000;

const URL = config.url;

const httpServer = http.createServer(app);

db.connect('mongodb://' + URL + ':27017/test-app', (err) => {
    if (err) {
        console.log('Error db connection: ', err);
        return;
    }

    httpServer.listen(PORT_HTTP, () => {
        console.log("http server running at http://" + URL + ":" + PORT_HTTP + '/');
    });

});


