export class Constants {
  public static _userRoles = new Map([
    [1, 'user'],
    [2, 'admin']
  ]);

  public static get UrlApi(): string {
    return 'http://localhost:3000'; // po idee zdes mojno cheerez proxy.conf ili je cherez environments sdelat'
  }
  public static UserRole(index): string {
    return this._userRoles.get(index);
  }
}
