import {DynamicForm} from './DynamicForm';

export class TextboxInput extends DynamicForm<string> {
  controlType = 'textbox';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
