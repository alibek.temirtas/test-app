import {DynamicForm} from './DynamicForm';

export class TextAreaInput extends DynamicForm<string> {
  controlType = 'textarea';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
