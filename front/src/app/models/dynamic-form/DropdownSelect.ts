import {DynamicForm} from './DynamicForm';

export class DropdownSelect extends DynamicForm<string> {
  controlType = 'dropdown';
  options: { key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}
