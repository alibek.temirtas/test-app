export class Card {
  readonly _id: string;
  readonly createdDate: number | null;
  readonly deletedDate: number | null;
  readonly isDeleted: boolean;
  title: string;
  desc: string;

  constructor(options: {
    title?: string,
    desc?: string;
  } = {}) {
    this.title = options.title;
    this.desc = options.desc || '';
  }
}
