import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

// APP MODULES :
import {SharedModule} from '../../shared/shared.module';

// COMPONENTS :
import {RootHomeComponent} from './_root/component';
import {MainHomeComponent} from './main/component';
import {DetailHomeComponent} from './detail/component';
import {CreateHomeComponent} from './create/component';

import {HeaderUiComponent} from './ui/header/component';

export const routes: Routes = [
  {
    path: '', component: RootHomeComponent, data: {state: ''},
    children: [
      {
        path: 'home', component: MainHomeComponent, data: {state: 'home'}
      },
      {
        path: 'create', component: CreateHomeComponent, data: {state: 'create'}
      },
      {
        path: 'detail/:id', component: DetailHomeComponent, data: {state: 'detail'}
      },
      {
        path: '', redirectTo: 'home', pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  declarations: [
    RootHomeComponent,
    MainHomeComponent,
    DetailHomeComponent,
    CreateHomeComponent,
    HeaderUiComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports: [
    HeaderUiComponent
  ],
  providers: [
  ]
})
export class HomeModule {
}
