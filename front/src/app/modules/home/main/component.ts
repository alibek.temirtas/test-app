import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup} from "@angular/forms";

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {CardsService} from '../../../shared/services/cards.service';
import {DynamicFormService} from "../../../shared/services/dynamic-form.service";
import {AlertService} from "../../../shared/services/alert.service";

import {Card} from '../../../models/Card';

import {TextboxInput} from '../../../models/dynamic-form/TextboxInput';
import {TextAreaInput} from '../../../models/dynamic-form/TextArea';

@Component({
  selector: 'app-home-main-page',
  templateUrl: './view.html'
})
export class MainHomeComponent implements OnInit, OnDestroy {
  cards: Card[];
  cardId: string;
  editedCard: Card;

  private _onDestroy = new Subject();

  editFormElements: any;
  editForm: FormGroup;

  // PAGINATION :
  pages = [];
  pageCount;
  pageSize = 5;
  currentPage = 1;
  itemCount;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private _cardService: CardsService,
              private _dynamicFormService: DynamicFormService,
              private _alertService: AlertService) {

  }

  ngOnInit(): void {
    this.getData(this.currentPage, this.pageSize);
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
  }

  getData(page = null, pageSize = null): void {
    this._cardService.getCardList(page, pageSize)
      .pipe(takeUntil(this._onDestroy))
      .subscribe((data: any) => {
        this.pages = [];
        this.cards = data.items;
        this.addPages(1, data.pages);
        this.itemCount = data.count;
        console.log('data: ', data);
      }, error => {

      });
  }

  deleteCard(formdata: { card: Card, index: number }) {
    let {card} = formdata;
    this.cards = this.cards.filter((item) => {
      return !(item._id === card._id);
    });


    if (confirm('Удалить ?')) {
      this._cardService.deleteCard({cardId: card._id}).pipe(takeUntil(this._onDestroy)).subscribe((data) => {
        console.log('data: ', data);


        // po horowemu zdes nujno sdelat nujno synchronno promise naprimer
        if (this.cards.length <= 0) {
          this.selectPage(this.currentPage - 1);
        }

      });
    }
  }

  editCard(formdata: { card: Card, index: number }) {
    let {card} = formdata;
    this.editFormElements = [
      new TextboxInput({
        key: 'title',
        label: 'Заголовок',
        required: true,
        value: card.title,
        type: 'text'
      }),
      new TextAreaInput({
        key: 'desc',
        label: 'Описание',
        required: true,
        value: card.desc,
        type: 'text'
      })
    ];

    this.cardId = card._id;
    this.editedCard = card;
    this.editForm = this._dynamicFormService.toFormGroup(this.editFormElements);

    console.log('edit card: ', formdata);
    $('#editCardModal').modal('show');
  }

  updateCard(){
    const controls = this.editForm.controls;
    if (this.editForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    let formData: any = {};
    Object.assign(formData, {cardId: this.cardId}, this.editForm.value);
    this._cardService.updateCard(formData).pipe(takeUntil(this._onDestroy)).subscribe((data) => {
      $('#editCardModal').modal('hide');
      this.editedCard.title = formData.title;
      this.editedCard.desc = formData.desc;
      this._alertService.success('Успешно обновлено !');
      this.editForm.reset({});
    }, err => {
      // zdes' po horowemu nujno owibku po statusus obrabotat' ));
    });
  }

  // PAGINATION :
  /*
    @param: number
    @param: number
   */
  addPages(from, to): void {
    for (let i = from; i <= to; i++) {
      this.pages.push(i);
    }
  }

  selectPage(page) {
    this.getData(page, this.pageSize);
    this.currentPage = page;
  }

  setPageSize(pageSize) {
    this.pageSize = pageSize;
    this.getData(1, this.pageSize);
  }
}
