import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-home-detail-page',
  templateUrl: './view.html'
})
export class DetailHomeComponent implements OnInit, OnDestroy {

  private _onDestroy = new Subject();

  constructor(private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
  }

}
