import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DynamicFormService} from '../../../shared/services/dynamic-form.service';
import {AlertService} from "../../../shared/services/alert.service";
import {CardsService} from "../../../shared/services/cards.service";

import {TextboxInput} from '../../../models/dynamic-form/TextboxInput';
import {TextAreaInput} from '../../../models/dynamic-form/TextArea';

@Component({
  selector: 'app-home-create-page',
  templateUrl: './view.html'
})
export class CreateHomeComponent implements OnInit, OnDestroy {
  cardFormGroup: FormGroup;

  //
  files: File[] = [];
  file: File;

  formElements = [
    new TextboxInput({
      key: 'title',
      label: 'Заголовок',
      required: true,
      value: null,
      type: 'text'
    }),
    new TextAreaInput({
      key: 'desc',
      label: 'Описание',
      required: true,
      value: null,
      type: 'text'
    })
  ];

  private _onDestroy = new Subject();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private _dynamicFormService: DynamicFormService,
              private _cardService: CardsService,
              private _alertService: AlertService) {

  }

  ngOnInit(): void {
    this.initForms();
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
  }

  public initForms(): void {
    this.cardFormGroup = this._dynamicFormService.toFormGroup(this.formElements);
  }

  createCard(): void {
    const controls = this.cardFormGroup.controls;
    if (this.cardFormGroup.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    let formData: any = {};
    Object.assign(formData, this.cardFormGroup.value);

    this._cardService.createCard(formData, this.files).pipe(takeUntil(this._onDestroy)).subscribe((data) => {
      this._alertService.success('Карточка успешно создана!');
      this.cardFormGroup.reset({});
    });

  }

  handleFileInput(_file: any) {
    this.file = _file.item(0);
    this.files.push(this.file);
    console.log('file: ', this.file);
  }

  selectFile() {
    let file = document.getElementById('file');
    file.click();
  }

  removeFile(index) {
    $('#file').val('');
    this.files.splice(index, 1);
  }

}
