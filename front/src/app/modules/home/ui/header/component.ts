import {Component, OnInit, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-ui-header',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class HeaderUiComponent implements OnInit, OnDestroy {

  constructor() {

  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
  }

}
