import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {DynamicForm} from '../../models/dynamic-form/DynamicForm';

@Injectable()
export class DynamicFormService {
  constructor() {
  }

  toFormGroup(elements: DynamicForm<any>[]) {
    let group: any = {};

    elements.forEach(item => {
      group[item.key] = item.required ?
        new FormControl({value: item.value || null, disabled: item.disabled || false}, Validators.required)
        : new FormControl({value: item.value || null, disabled: item.disabled || false});
    });
    return new FormGroup(group);
  }
}
