import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Constants} from '../../constants/constants';
import {Card} from '../../models/Card';

@Injectable()
export class CardsService {
  private _serverUrl = Constants.UrlApi;

  constructor(private _http: HttpClient) {
  }

  createCard(_formData: any, _files): Observable<Card[]> {

    let formData = new FormData();

    _files.forEach((item) => {
      formData.append('uploads[]', item);
    });

    formData.append('title', _formData.title);
    formData.append('desc', _formData.desc);

    return this._http.post(`${this._serverUrl}/api/card`, formData).pipe(
      map((response: any) => {
        return response;
      }),
    );
  }

  getCardList(page = null, pageSize = null): Observable<Card[]> {
    let url = '';

    if (pageSize) {
      url += 'page_size=' + pageSize + '&';
    }

    return this._http.get(`${this._serverUrl}/api/card${page > 0 ? '?page=' + page + '&' : '?'}${url}`, {}).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  updateCard(formData: any): Observable<any> {
    return this._http.put(`${this._serverUrl}/api/card`, formData).pipe(
      map((response: any) => {
        return response;
      }),
    );
  }

  deleteCard(formData: {cardId: string}): Observable<any> {
    let postJsonData = JSON.stringify(formData);
    return this._http.delete(`${this._serverUrl}/api/card`,{params: formData}).pipe(
      map((response: any) => {
        return response;
      }),
    );
  }

}
