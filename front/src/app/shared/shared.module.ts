import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

// SERVICES :
import {AlertService} from './services/alert.service';
import {DynamicFormService} from './services/dynamic-form.service';
import {CardsService} from './services/cards.service';

// COMPONENTS :
import {AlertComponent} from './components/alert/alert.component';
import {DynamicFormComponent} from './components/form-elements/component';
import {CardListComponent} from './components/card-list/component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    AlertComponent,
    DynamicFormComponent,
    CardListComponent
  ],
  providers: [
    AlertService,
    DynamicFormService,
    CardsService
  ],
  exports: [
    RouterModule,
    HttpClientModule,
    AlertComponent,
    DynamicFormComponent,
    CardListComponent,
    FormsModule,
    ReactiveFormsModule,
  ]
})

export class SharedModule {
}
