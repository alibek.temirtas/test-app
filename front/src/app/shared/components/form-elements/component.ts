import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {DynamicForm} from '../../../models/dynamic-form/DynamicForm';

@Component({
  selector: 'app-form-element',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class DynamicFormComponent implements OnInit {
  @Input() element?: DynamicForm<any>;
  @Input() form?: FormGroup;
  @Input() label?: boolean;

  ngOnInit() {
  }
}
