import {Component, OnInit, OnDestroy, Input, Output, EventEmitter} from '@angular/core';

import {Constants} from "../../../constants/constants";
import {Card} from '../../../models/Card';

@Component({
  selector: 'app-card-list',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class CardListComponent implements OnInit, OnDestroy {
  @Input() items: Card[];

  @Output() onDeleteCard: EventEmitter<any> = new EventEmitter();
  @Output() onEditCard: EventEmitter<any> = new EventEmitter();

  url: string = Constants.UrlApi;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  deleteItem(card, index) {
    let data = {
      card: card,
      index: index
    };
    this.onDeleteCard.emit(data);
  }

  editItem(card, index){
    let data = {
      card: card,
      index: index
    };
    this.onEditCard.emit(data);
  }
}
