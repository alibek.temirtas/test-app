import {Component, ViewChild, ElementRef, Renderer2, OnInit, OnDestroy} from '@angular/core';
import {AlertService} from '../../services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {
  @ViewChild('alert') alert: ElementRef;

  message: any;
  // Subsribtions :
  sub: any;

  constructor(private _alertService: AlertService,
              public renderer: Renderer2) {
  }

  ngOnInit() {
    this.sub = this._alertService.getMessage().subscribe(message => {
      this.message = message;
    });
  }

  ngOnDestroy() {
    this.sub ? this.sub.unsubscribe() : console.log('destroyed');
  }

  close(): void {
    this.destroy();
  }

  private destroy(): void {
    this.renderer.addClass(this.alert.nativeElement, 'bounceOut');
    setTimeout(() => {
      this.message = '';
    }, 1000);
  }

}
